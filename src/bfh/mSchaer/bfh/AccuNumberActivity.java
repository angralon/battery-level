package bfh.mSchaer.bfh;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.util.Log;

public class AccuNumberActivity extends Activity
{
	/** Called when the activity is first created. */
	private static final int HELLO_ID = 1;

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		//batteryNotify(10);
		this.registerReceiver(this.batteryInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
	}

	private BroadcastReceiver batteryInfoReceiver = new BroadcastReceiver() 
	{
		@Override
		public void onReceive(Context context, Intent intent) 
		{
			int  level= intent.getIntExtra(BatteryManager.EXTRA_LEVEL,0);
			batteryNotify(context, level);
			Log.d("Battery", String.valueOf(getWindowManager().getDefaultDisplay().getWidth())); 
		}
	};

	private void batteryNotify(Context context, int level)
	{
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);

		int icon = selectImage(level);
		CharSequence tickerText = "";
		long when = System.currentTimeMillis();

		Notification notification = new Notification(icon, tickerText, when);

		CharSequence contentTitle = "Battery level: "+String.valueOf(level);
		CharSequence contentText = "";
		Intent notificationIntent = new Intent(this, AccuNumberActivity.class);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

		notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);


		mNotificationManager.notify(HELLO_ID, notification);

/*
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
		int icon = R.drawable.vide; 
		CharSequence tickerText = String.valueOf(level);
		long when = System.currentTimeMillis();
		final int CUSTOM_VIEW_ID = 1;

		Notification notification = new Notification(icon, tickerText, when);

		RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.main);
		contentView.setImageViewResource(icon, icon);
		contentView.setTextViewText(icon, "Racquetime \n Message");

		notification.contentView = contentView;


		notification.defaults=Notification.FLAG_ONLY_ALERT_ONCE+Notification.FLAG_AUTO_CANCEL;
		Intent notificationIntent;
		

		notificationIntent = new Intent(this,AccuNumberActivity.class);

		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.defaults |= Notification.DEFAULT_SOUND;
		notification.flags |= Notification.FLAG_SHOW_LIGHTS;

		notificationIntent.putExtra("Tag", "C2DMBaseReceiver");
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		notification.contentIntent = contentIntent;
		mNotificationManager.notify(CUSTOM_VIEW_ID, notification);
*/
	}
	
	private int selectImage(int level)
	{
		if(level <= 10)
		{
			return R.drawable.level1;
		}
		else if (level <= 25)
		{
			return R.drawable.level2;
		}
		else if (level <= 50)
		{
			return R.drawable.level3;
		}
		else if (level <= 75)
		{
			return R.drawable.level4;
		}
		else if (level <= 100)
		{
			return R.drawable.level5;
		}
		else return R.drawable.vide;
	}

}